package com.example.realworld.base.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.ZonedDateTime;

@MappedSuperclass
@Getter
public abstract class DateEntity {

    @CreatedDate
    @Column(name = "created_at", updatable = false)
    protected ZonedDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at")
    protected ZonedDateTime updatedAt;

    @PrePersist
    void prePersist() {
        this.createdAt = ZonedDateTime.now();
        this.updatedAt = ZonedDateTime.now();
    }

    @PreUpdate
    void preUpdate() {
        updatedAt = ZonedDateTime.now();
    }

}
