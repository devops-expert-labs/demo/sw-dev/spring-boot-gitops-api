package com.example.realworld.domain.aggregate.profile.repository;

import com.example.realworld.domain.aggregate.profile.entity.Follow;
import com.example.realworld.domain.aggregate.profile.entity.FollowId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Follow, FollowId> {

    Optional<Follow> findByFolloweeIdAndFollowerId(Long followeeId, Long followerId);

    List<Follow> findByFolloweeId(Long followerId);

    List<Follow> findByFollowerId(Long followerId);

}
