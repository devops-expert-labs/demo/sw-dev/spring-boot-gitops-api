package com.example.realworld.domain.aggregate.tag.service;

import com.example.realworld.domain.aggregate.tag.entity.Tag;
import com.example.realworld.domain.aggregate.tag.repository.TagRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Override
    public List<String> getTags() {
        return tagRepository.findAll().stream().map(Tag::getName).distinct().collect(Collectors.toList());
    }

}
