package com.example.realworld.domain.aggregate.tag.controller;

import com.example.realworld.domain.aggregate.tag.dto.TagResponse;
import com.example.realworld.domain.aggregate.tag.service.TagService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tags")
public class TagController {

    private final TagService tagService;

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    public TagResponse getTags() {
        return TagResponse.builder().tags(tagService.getTags()).build();
    }
}
